﻿
namespace hmsvid
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddPatient = new System.Windows.Forms.Button();
            this.btnGeneralPhysicalExam = new System.Windows.Forms.Button();
            this.btnPreOrderDiagnosis = new System.Windows.Forms.Button();
            this.btnDiagnosis = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnTreatment = new System.Windows.Forms.Button();
            this.btnFullHistoryofPatient = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.labelind1 = new System.Windows.Forms.Label();
            this.labelind2 = new System.Windows.Forms.Label();
            this.labelind3 = new System.Windows.Forms.Label();
            this.labelind4 = new System.Windows.Forms.Label();
            this.labelind5 = new System.Windows.Forms.Label();
            this.labelind6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.txtCNIC = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxGender = new System.Windows.Forms.ComboBox();
            this.comboboxBloodGroup = new System.Windows.Forms.ComboBox();
            this.txtPatientAge = new System.Windows.Forms.TextBox();
            this.txtPatientName = new System.Windows.Forms.TextBox();
            this.txtRefferedBy = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxCity = new System.Windows.Forms.ComboBox();
            this.comboBoxProvince = new System.Windows.Forms.ComboBox();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPatientContactNo = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(532, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(522, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hospital Management System";
            // 
            // btnAddPatient
            // 
            this.btnAddPatient.BackColor = System.Drawing.Color.LightGray;
            this.btnAddPatient.Location = new System.Drawing.Point(42, 240);
            this.btnAddPatient.Name = "btnAddPatient";
            this.btnAddPatient.Size = new System.Drawing.Size(135, 30);
            this.btnAddPatient.TabIndex = 1;
            this.btnAddPatient.Text = "Add New Patient";
            this.btnAddPatient.UseVisualStyleBackColor = false;
            this.btnAddPatient.Click += new System.EventHandler(this.btnAddPatient_Click);
            // 
            // btnGeneralPhysicalExam
            // 
            this.btnGeneralPhysicalExam.BackColor = System.Drawing.Color.LightGray;
            this.btnGeneralPhysicalExam.Location = new System.Drawing.Point(42, 276);
            this.btnGeneralPhysicalExam.Name = "btnGeneralPhysicalExam";
            this.btnGeneralPhysicalExam.Size = new System.Drawing.Size(135, 30);
            this.btnGeneralPhysicalExam.TabIndex = 2;
            this.btnGeneralPhysicalExam.Text = "General Physical Exam";
            this.btnGeneralPhysicalExam.UseVisualStyleBackColor = false;
            this.btnGeneralPhysicalExam.Click += new System.EventHandler(this.btnGeneralPhysicalExam_Click);
            // 
            // btnPreOrderDiagnosis
            // 
            this.btnPreOrderDiagnosis.BackColor = System.Drawing.Color.LightGray;
            this.btnPreOrderDiagnosis.Location = new System.Drawing.Point(42, 312);
            this.btnPreOrderDiagnosis.Name = "btnPreOrderDiagnosis";
            this.btnPreOrderDiagnosis.Size = new System.Drawing.Size(135, 30);
            this.btnPreOrderDiagnosis.TabIndex = 3;
            this.btnPreOrderDiagnosis.Text = "Pre-Order Diagnosis";
            this.btnPreOrderDiagnosis.UseVisualStyleBackColor = false;
            this.btnPreOrderDiagnosis.Click += new System.EventHandler(this.btnPreOrderDiagnosis_Click);
            // 
            // btnDiagnosis
            // 
            this.btnDiagnosis.BackColor = System.Drawing.Color.LightGray;
            this.btnDiagnosis.Location = new System.Drawing.Point(42, 348);
            this.btnDiagnosis.Name = "btnDiagnosis";
            this.btnDiagnosis.Size = new System.Drawing.Size(135, 30);
            this.btnDiagnosis.TabIndex = 4;
            this.btnDiagnosis.Text = "Diagnosis";
            this.btnDiagnosis.UseVisualStyleBackColor = false;
            this.btnDiagnosis.Click += new System.EventHandler(this.btnDiagnosis_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Minion Pro", 22F);
            this.label2.Location = new System.Drawing.Point(41, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 40);
            this.label2.TabIndex = 5;
            this.label2.Text = "Controller";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(23, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(167, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btnTreatment
            // 
            this.btnTreatment.BackColor = System.Drawing.Color.LightGray;
            this.btnTreatment.Location = new System.Drawing.Point(42, 385);
            this.btnTreatment.Name = "btnTreatment";
            this.btnTreatment.Size = new System.Drawing.Size(135, 30);
            this.btnTreatment.TabIndex = 7;
            this.btnTreatment.Text = "Treatment";
            this.btnTreatment.UseVisualStyleBackColor = false;
            this.btnTreatment.Click += new System.EventHandler(this.btnTreatment_Click);
            // 
            // btnFullHistoryofPatient
            // 
            this.btnFullHistoryofPatient.BackColor = System.Drawing.Color.LightGray;
            this.btnFullHistoryofPatient.Location = new System.Drawing.Point(42, 422);
            this.btnFullHistoryofPatient.Name = "btnFullHistoryofPatient";
            this.btnFullHistoryofPatient.Size = new System.Drawing.Size(135, 30);
            this.btnFullHistoryofPatient.TabIndex = 8;
            this.btnFullHistoryofPatient.Text = "Full History of Patients";
            this.btnFullHistoryofPatient.UseVisualStyleBackColor = false;
            this.btnFullHistoryofPatient.Click += new System.EventHandler(this.btnFullHistoryofPatient_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(435, 197);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(689, 335);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(67, 489);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(12, 518);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(210, 125);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 11;
            this.pictureBox3.TabStop = false;
            // 
            // labelind1
            // 
            this.labelind1.AutoSize = true;
            this.labelind1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelind1.Location = new System.Drawing.Point(8, 241);
            this.labelind1.Name = "labelind1";
            this.labelind1.Size = new System.Drawing.Size(32, 24);
            this.labelind1.TabIndex = 12;
            this.labelind1.Text = ">>";
            // 
            // labelind2
            // 
            this.labelind2.AutoSize = true;
            this.labelind2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelind2.Location = new System.Drawing.Point(8, 277);
            this.labelind2.Name = "labelind2";
            this.labelind2.Size = new System.Drawing.Size(32, 24);
            this.labelind2.TabIndex = 13;
            this.labelind2.Text = ">>";
            // 
            // labelind3
            // 
            this.labelind3.AutoSize = true;
            this.labelind3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelind3.Location = new System.Drawing.Point(8, 313);
            this.labelind3.Name = "labelind3";
            this.labelind3.Size = new System.Drawing.Size(32, 24);
            this.labelind3.TabIndex = 14;
            this.labelind3.Text = ">>";
            // 
            // labelind4
            // 
            this.labelind4.AutoSize = true;
            this.labelind4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelind4.Location = new System.Drawing.Point(8, 349);
            this.labelind4.Name = "labelind4";
            this.labelind4.Size = new System.Drawing.Size(32, 24);
            this.labelind4.TabIndex = 15;
            this.labelind4.Text = ">>";
            // 
            // labelind5
            // 
            this.labelind5.AutoSize = true;
            this.labelind5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelind5.Location = new System.Drawing.Point(8, 386);
            this.labelind5.Name = "labelind5";
            this.labelind5.Size = new System.Drawing.Size(32, 24);
            this.labelind5.TabIndex = 16;
            this.labelind5.Text = ">>";
            // 
            // labelind6
            // 
            this.labelind6.AutoSize = true;
            this.labelind6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelind6.Location = new System.Drawing.Point(8, 423);
            this.labelind6.Name = "labelind6";
            this.labelind6.Size = new System.Drawing.Size(32, 24);
            this.labelind6.TabIndex = 17;
            this.labelind6.Text = ">>";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPatientContactNo);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.txtCNIC);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.comboBoxGender);
            this.panel1.Controls.Add(this.comboboxBloodGroup);
            this.panel1.Controls.Add(this.txtPatientAge);
            this.panel1.Controls.Add(this.txtPatientName);
            this.panel1.Controls.Add(this.txtRefferedBy);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(435, 197);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1077, 636);
            this.panel1.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(228, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Add without Dash";
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(870, 576);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 42);
            this.buttonSave.TabIndex = 15;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // txtCNIC
            // 
            this.txtCNIC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNIC.Location = new System.Drawing.Point(131, 88);
            this.txtCNIC.Name = "txtCNIC";
            this.txtCNIC.Size = new System.Drawing.Size(313, 24);
            this.txtCNIC.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(27, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 18);
            this.label11.TabIndex = 13;
            this.label11.Text = "Patient CNIC";
            // 
            // comboBoxGender
            // 
            this.comboBoxGender.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxGender.FormattingEnabled = true;
            this.comboBoxGender.Items.AddRange(new object[] {
            "Male",
            "Female",
            "Other"});
            this.comboBoxGender.Location = new System.Drawing.Point(131, 177);
            this.comboBoxGender.Name = "comboBoxGender";
            this.comboBoxGender.Size = new System.Drawing.Size(121, 26);
            this.comboBoxGender.TabIndex = 12;
            // 
            // comboboxBloodGroup
            // 
            this.comboboxBloodGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboboxBloodGroup.FormattingEnabled = true;
            this.comboboxBloodGroup.Items.AddRange(new object[] {
            "A+ (A positive)",
            "A− (A negative) ",
            "B+ (B positive)",
            "B− (B negative)",
            "AB+ (AB positive)",
            "AB− (AB negative)",
            "O+ (O positive)",
            "O− (O negative)"});
            this.comboboxBloodGroup.Location = new System.Drawing.Point(632, 178);
            this.comboboxBloodGroup.Name = "comboboxBloodGroup";
            this.comboboxBloodGroup.Size = new System.Drawing.Size(313, 26);
            this.comboboxBloodGroup.TabIndex = 11;
            // 
            // txtPatientAge
            // 
            this.txtPatientAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientAge.Location = new System.Drawing.Point(632, 134);
            this.txtPatientAge.Name = "txtPatientAge";
            this.txtPatientAge.Size = new System.Drawing.Size(313, 24);
            this.txtPatientAge.TabIndex = 10;
            // 
            // txtPatientName
            // 
            this.txtPatientName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientName.Location = new System.Drawing.Point(131, 134);
            this.txtPatientName.Name = "txtPatientName";
            this.txtPatientName.Size = new System.Drawing.Size(313, 24);
            this.txtPatientName.TabIndex = 9;
            // 
            // txtRefferedBy
            // 
            this.txtRefferedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefferedBy.Location = new System.Drawing.Point(131, 43);
            this.txtRefferedBy.Name = "txtRefferedBy";
            this.txtRefferedBy.Size = new System.Drawing.Size(313, 24);
            this.txtRefferedBy.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxCity);
            this.groupBox1.Controls.Add(this.comboBoxProvince);
            this.groupBox1.Controls.Add(this.txtArea);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 226);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(942, 328);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address";
            // 
            // comboBoxCity
            // 
            this.comboBoxCity.FormattingEnabled = true;
            this.comboBoxCity.Items.AddRange(new object[] {
            "Abbotabad",
            "Ahmedpur East",
            "Arif Wala",
            "Attock",
            "Badin",
            "Bahawalnagar",
            "Bahawalpur",
            "Bhakkar",
            "Bhalwal",
            "Burewala",
            "Chakwal",
            "Chaman",
            "Charsadda",
            "Chiniot",
            "Chishtian",
            "Dadu",
            "Daharki",
            "Daska",
            "Dera Ghazi Khan",
            "Dera Ismail Khan",
            "Faisalabad",
            "Ferozwala",
            "Ghotki",
            "Gojra",
            "Gujranwala",
            "Gujranwala Cantonment",
            "Gujrat",
            "Hafizabad",
            "Haroonabad",
            "Hasilpur",
            "Hub",
            "Hyderabad",
            "Islamabad",
            "Jacobabad",
            "Jaranwala",
            "Jatoi",
            "Jhang",
            "Jhelum",
            "Kabal",
            "Kamalia",
            "Kamber Ali Khan",
            "Kāmoke",
            "Kandhkot",
            "Karachi",
            "Kasur",
            "Khairpur",
            "Khanewal",
            "Khanpur",
            "Khushab",
            "Khuzdar",
            "Kohat",
            "Kot Abdul Malik",
            "Kot Addu",
            "Kotri",
            "Lahore",
            "Larkana",
            "Layyah",
            "Lodhran",
            "Mandi Bahauddin",
            "Mansehra",
            "Mardan",
            "Mianwali",
            "Mingora",
            "Mirpur",
            "Mirpur Khas",
            "Mirpur Mathelo",
            "Multan",
            "Muridke",
            "Muzaffarabad",
            "Muzaffargarh",
            "Narowal",
            "Nawabshah",
            "Nowshera",
            "Okara",
            "Pakpattan",
            "Peshawar",
            "Quetta",
            "Rahim Yar Khan",
            "Rawalpindi",
            "Sadiqabad",
            "Sahiwal",
            "Sambrial",
            "Samundri",
            "Sargodha",
            "Shahdadkot",
            "Sheikhupura",
            "Shikarpur",
            "Sialkot",
            "Sukkur",
            "Swabi",
            "Tando Adam",
            "Tando Allahyar",
            "Tando Muhammad Khan",
            "Taxila",
            "Turbat",
            "Umerkot",
            "Vehari",
            "Wah Cantonment",
            "Wazirabad"});
            this.comboBoxCity.Location = new System.Drawing.Point(548, 43);
            this.comboBoxCity.Name = "comboBoxCity";
            this.comboBoxCity.Size = new System.Drawing.Size(330, 26);
            this.comboBoxCity.TabIndex = 15;
            // 
            // comboBoxProvince
            // 
            this.comboBoxProvince.FormattingEnabled = true;
            this.comboBoxProvince.Items.AddRange(new object[] {
            "Capital",
            "Punjab",
            "Sindh",
            "Balochistan",
            "KPK",
            "Azad Kashmir"});
            this.comboBoxProvince.Location = new System.Drawing.Point(102, 43);
            this.comboBoxProvince.Name = "comboBoxProvince";
            this.comboBoxProvince.Size = new System.Drawing.Size(339, 26);
            this.comboBoxProvince.TabIndex = 14;
            // 
            // txtArea
            // 
            this.txtArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtArea.Location = new System.Drawing.Point(102, 161);
            this.txtArea.Multiline = true;
            this.txtArea.Name = "txtArea";
            this.txtArea.Size = new System.Drawing.Size(339, 151);
            this.txtArea.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 18);
            this.label10.TabIndex = 8;
            this.label10.Text = "Province";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 18);
            this.label9.TabIndex = 6;
            this.label9.Text = "Area";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(486, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 18);
            this.label8.TabIndex = 5;
            this.label8.Text = "City";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(503, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "Blood Group";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(503, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = "Patient Age";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(27, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 18);
            this.label5.TabIndex = 2;
            this.label5.Text = "Gender";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "Patient Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(27, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Reffered By";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(487, 84);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 36);
            this.label13.TabIndex = 17;
            this.label13.Text = "Patient Contact\r\n      Number";
            // 
            // txtPatientContactNo
            // 
            this.txtPatientContactNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatientContactNo.Location = new System.Drawing.Point(632, 85);
            this.txtPatientContactNo.Name = "txtPatientContactNo";
            this.txtPatientContactNo.Size = new System.Drawing.Size(313, 24);
            this.txtPatientContactNo.TabIndex = 18;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.checkBox4);
            this.panel2.Controls.Add(this.checkBox3);
            this.panel2.Controls.Add(this.checkBox2);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Location = new System.Drawing.Point(435, 197);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1077, 636);
            this.panel2.TabIndex = 19;
            this.panel2.Visible = false;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(348, 77);
            this.label14.TabIndex = 0;
            this.label14.Text = "General Physical Exam";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(8, 64);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(55, 24);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "GIT";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(8, 117);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(198, 24);
            this.checkBox2.TabIndex = 2;
            this.checkBox2.Text = "Central Nervous System";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3.Location = new System.Drawing.Point(8, 177);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(226, 24);
            this.checkBox3.TabIndex = 3;
            this.checkBox3.Text = "Respiratory System (Cardio)";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox4.Location = new System.Drawing.Point(8, 245);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(129, 24);
            this.checkBox4.TabIndex = 4;
            this.checkBox4.Text = "Muscoskeletal";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(870, 576);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 42);
            this.button1.TabIndex = 16;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.textBox8);
            this.panel3.Controls.Add(this.textBox7);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.textBox6);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.textBox5);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.textBox4);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Location = new System.Drawing.Point(435, 197);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1074, 634);
            this.panel3.TabIndex = 17;
            this.panel3.Visible = false;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 5);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(348, 35);
            this.label15.TabIndex = 1;
            this.label15.Text = "Pre Order Diagnosis";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(4, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 20);
            this.label16.TabIndex = 2;
            this.label16.Text = "CBC";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(8, 239);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 20);
            this.label17.TabIndex = 3;
            this.label17.Text = "LFTs";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(8, 413);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 20);
            this.label18.TabIndex = 20;
            this.label18.Text = "RFTs";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(8, 88);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(432, 148);
            this.textBox1.TabIndex = 21;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(8, 262);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(432, 148);
            this.textBox2.TabIndex = 22;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(8, 436);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(432, 148);
            this.textBox3.TabIndex = 23;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.label24.Location = new System.Drawing.Point(4, 39);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(119, 22);
            this.label24.TabIndex = 20;
            this.label24.Text = "Investigations";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(585, 308);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(163, 20);
            this.textBox8.TabIndex = 33;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(585, 252);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(163, 20);
            this.textBox7.TabIndex = 32;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(519, 308);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(40, 20);
            this.label23.TabIndex = 31;
            this.label23.Text = "AFP";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(516, 252);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 20);
            this.label22.TabIndex = 30;
            this.label22.Text = "CA19-9";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(585, 194);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(163, 20);
            this.textBox6.TabIndex = 29;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(517, 194);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 20);
            this.label21.TabIndex = 28;
            this.label21.Text = "CA 125";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(585, 136);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(163, 20);
            this.textBox5.TabIndex = 27;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(519, 135);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(42, 20);
            this.label20.TabIndex = 26;
            this.label20.Text = "CEA";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(585, 78);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(163, 20);
            this.textBox4.TabIndex = 25;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(517, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 20);
            this.label19.TabIndex = 24;
            this.label19.Text = "LDH";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.label25.Location = new System.Drawing.Point(517, 39);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(119, 22);
            this.label25.TabIndex = 34;
            this.label25.Text = "Investigations";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(870, 576);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 42);
            this.button2.TabIndex = 35;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.textBox9);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Location = new System.Drawing.Point(402, 197);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1137, 645);
            this.panel4.TabIndex = 36;
            this.panel4.Visible = false;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(348, 35);
            this.label26.TabIndex = 2;
            this.label26.Text = "Treatment";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(8, 39);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(1033, 499);
            this.textBox9.TabIndex = 3;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1540, 845);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelind6);
            this.Controls.Add(this.labelind5);
            this.Controls.Add(this.labelind4);
            this.Controls.Add(this.labelind3);
            this.Controls.Add(this.labelind2);
            this.Controls.Add(this.labelind1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnFullHistoryofPatient);
            this.Controls.Add(this.btnTreatment);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDiagnosis);
            this.Controls.Add(this.btnPreOrderDiagnosis);
            this.Controls.Add(this.btnGeneralPhysicalExam);
            this.Controls.Add(this.btnAddPatient);
            this.Controls.Add(this.label1);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddPatient;
        private System.Windows.Forms.Button btnGeneralPhysicalExam;
        private System.Windows.Forms.Button btnPreOrderDiagnosis;
        private System.Windows.Forms.Button btnDiagnosis;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnTreatment;
        private System.Windows.Forms.Button btnFullHistoryofPatient;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labelind1;
        private System.Windows.Forms.Label labelind2;
        private System.Windows.Forms.Label labelind3;
        private System.Windows.Forms.Label labelind4;
        private System.Windows.Forms.Label labelind5;
        private System.Windows.Forms.Label labelind6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboboxBloodGroup;
        private System.Windows.Forms.TextBox txtPatientAge;
        private System.Windows.Forms.TextBox txtPatientName;
        private System.Windows.Forms.TextBox txtRefferedBy;
        private System.Windows.Forms.ComboBox comboBoxProvince;
        private System.Windows.Forms.TextBox txtArea;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxCity;
        private System.Windows.Forms.ComboBox comboBoxGender;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.TextBox txtCNIC;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPatientContactNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button2;
    }
}